package br.edu.estacio.principal;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.Tuple;

import br.edu.estacio.model.Compras;
import br.edu.estacio.model.Pessoa;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Compras");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		Pessoa p1 = new Pessoa();
		p1.setNome("Aline");
		p1.setSobrenome("Morais");
		p1.addCompras(new Compras("Arroz",17.50));
		p1.addCompras(new Compras("Feijão",5.80));

		em.persist(p1);
		em.getTransaction().commit();
				
		em.getTransaction().begin();
		Pessoa p2 = new Pessoa();
		p2.setNome("João");
		p2.setSobrenome("Alves");
		p2.addCompras(new Compras("Abacate",2.70));
		p2.addCompras(new Compras("Abacaxi",3.00));
		p2.addCompras(new Compras("Feijão",5.90));
		p2.addCompras(new Compras("Vinagre",1.80));
		
		em.persist(p2);
		em.getTransaction().commit();
		
		em.getTransaction().begin();
		Pessoa p3 = new Pessoa();
		p3.setNome("José");
		p3.setSobrenome("Machado");
		em.persist(p3);
		em.getTransaction().commit();
		
		//Pesquisas com Join explicito
		Query query1 = em.createQuery("Select p from Pessoa p join p.compras c where c.descricao = :descricao")
				.setParameter("descricao", "Abacaxi");
		Iterator<Pessoa> it = query1.getResultList().iterator();
		while (it.hasNext()) {
			Pessoa p = it.next();
			System.out.println("Nome: "+p.getNome());
			System.out.println("---------------Compras------------------");
			Iterator<Compras> itcp = p.getCompras().iterator();
			while (itcp.hasNext()) {
				Compras comps = itcp.next();
				System.out.println("Produto: "+comps.getDescricao()+" "+comps.getValor());
			}
		}
		
		//Pesquisas com Join explicito 2
		Query query2 = em.createQuery("Select p from Pessoa p join p.compras c where c.descricao = ?1")
				.setParameter(1, "Abacaxi");
		it = query2.getResultList().iterator();
		while (it.hasNext()) {
			Pessoa p = it.next();
			System.out.println("Nome: "+p.getNome());
			System.out.println("---------------Compras------------------");
			Iterator<Compras> itcp = p.getCompras().iterator();
			while (itcp.hasNext()) {
				Compras comps = itcp.next();
				System.out.println("Produto: "+comps.getDescricao()+" "+comps.getValor());
			}
		}
		
		//Pesquisas com Join explicito 3
		Query query3 = em.createQuery("Select p from Pessoa p Join fetch p.compras",Pessoa.class);
		List<Pessoa> tuples = query3.getResultList();
		for (Pessoa t: tuples) {
			System.out.println("Nome: "+t.getNome());
		}		
		
		em.close();
		emf.close();
	}

}

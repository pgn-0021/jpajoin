package br.edu.estacio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Compras {

	public Compras() {}
	
	public Compras(String descricao,double valor) {
		this.descricao=descricao;
		this.valor=valor;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String descricao;
	
	private double valor;

	@ManyToOne
	@JoinColumn(name="dono_id")
	private Pessoa dono;
	
	@Column(name="descricao",length=30)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}	
	
	public void addDono(Pessoa dono) {
		this.dono = dono;
	}
	
	public Pessoa getDono() {
		return dono;
	}
	
}

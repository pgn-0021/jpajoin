package br.edu.estacio.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Pessoa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private int id;
	
	private String nome;
	
	private String sobrenome;

	//Associação com Compras
	@OneToMany(mappedBy="dono",cascade=CascadeType.ALL)
	private Set<Compras> compras = new HashSet<Compras>();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
		
	public void addCompras(Compras compras) {
		this.compras.add(compras);
		compras.addDono(this);
	}
	
	public Collection<Compras> getCompras(){
		return compras;
	}
	
}
